package fr.sbr;

import java.util.HashMap;
import java.util.Map;
 
public class CheckOrder {
  
 private static final long serialVersionUID = 1L;
  
 Map<String, Order> warehouse = new HashMap<String, Order>()
 {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

{
   put("1265", new Order("AZ-1265", 100, 2011.9f));
   put("6522", new Order("AZ-6522", 10, 115.9f));
   put("8436", new Order("AZ-8436", 200, 500.10f));
  }
 };
  
 public String acceptOrder(Order payload)
 {
  payload.setResponse("Accepted");
  return payload.toString();
 }
  
 public String refuseOrder(Order payload)
 {
  payload.setResponse("Refused");
  return payload.toString();
 }
  
 public Order processOrder(String payload)
 {
  Order order = warehouse.get(payload);
  if (order == null)
  {
   //Order not found
   order = new Order("0", 0, 0);
  }
   
  return order;
 }
}

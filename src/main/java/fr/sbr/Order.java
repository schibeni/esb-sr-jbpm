package fr.sbr;
 
import java.io.Serializable;
 
public class Order implements Serializable {
 
 /**
  *
  */
 private static final long serialVersionUID = 7830120993371620210L;
  
 public String orderId;
 public int quantity;
 public float amount;
 private String response;
  
 public Order(String orderId,
     int quantity,
     float amount)
 {
  this.orderId = orderId;  
  this.quantity = quantity;
  this.amount = amount;
 }
  
 public String getOrderId() {
  return orderId;
 }
 public void setOrderId(String orderId) {
  this.orderId = orderId;
 }
 public int getQuantity() {
  return quantity;
 }
 public void setQuantity(int quantity) {
  this.quantity = quantity;
 }
 public float getAmount() {
  return amount;
 }
 public void setAmount(float amount) {
  this.amount = amount;
 }
  
 @Override
 public String toString()
 {
  return "orderId: " + orderId +
    " quantity: " + quantity +
    " amount: " + amount +
    " response:" + response ;
 }
 
 public String getResponse() {
  return response;
 }
 
 public void setResponse(String response) {
  this.response = response;
 }
 
}